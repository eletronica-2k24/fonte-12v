# Fonte-12V



## Participantes

Beatriz Alves dos Santos - 15588630<br>
Gustavo Ramos Santos Pires - 15458030<br>
Newton Eduardo Pena Villegas - 15481732<br>
Ryan Diniz Pereira - 15590907<br>

## Tabela de Componentes

| Componente         | Quantidade | Preço (R$) |
|--------------------|------------|------------|
| Resistor 120 ohm   | 2          | 2,50       |
| Resistor 2.2k ohm  | 1          | 0,30       |
| Resistor 1k ohm    | 2          | 0,20       |
| Resistor 470 ohm   | 1          | 0,10       |
| Transistor 2N3904  | 1          | 0,35       |
| Diodo Zener 13V    | 1          | 0,50       |

## Execução

Em um primeiro momento, adotamos uma abordagem para o projeto da fonte em um simulador(Falstad), usando como referência os projetos fornecidos pelo professor, fizemos uso de uma comodidade possibilitada pela ferramenta, um processo de refinamento contínuo, com ajustes sucessivos e aprimoramentos incrementais. Testes iterativos e correções adaptativas(tentamos até dar certo).

Uma vez que simulando o circuito, tudo funcionou conforme o esperado, passamos para a etapa de aquisição das peças físicas, depois para a montagem do circuito na breadboard/protoboard.

Vale citar que inicialmente, o circuito esquentava um pouco mais do que o esperado, mas que a disssipação de calor por vento já seria suficiente para que não houve problema com a entrega do produto final, mas optamos por aumentar um pouco a capacidade dos componentes que estavam causando o aquecimento, a fim de não precisar entregar o produto final com um dos membros do grupo soprando incessantemente o dispositivo.

### link do Falstad
https://tinyurl.com/279tpkvv

### link do video explicando os componentes da fonte
https://drive.google.com/file/d/1iDLgNgmPxSDEzOHVnZ6EofxoY6vUKHJK/view?usp=sharing